package test;

import java.io.IOException;

import org.testng.annotations.Test;

import BaseTest.BaseTest;
import Utilities.TakeScreenShot;
import pages.LoginPage;

public class LoginPage_test extends BaseTest{
	
	@Test
	public void loginTest() throws IOException{
	LoginPage lp=new LoginPage(driver);
	lp.enter_username("value");
	lp.enter_password("value");
	lp.clickLogin();
	TakeScreenShot.screenShot("loginpage");
	
	}

}
