package test;

import java.io.IOException;

import org.testng.annotations.Test;

import BaseTest.BaseTest;
import Utilities.TakeScreenShot;
import pages.BuyStockPage;

public class BuyStockPage_test extends BaseTest{
	
	@Test
	public void buyStockPageTest() throws IOException{
	BuyStockPage bsp=new BuyStockPage(driver);
	bsp.selectstock("value");
	bsp.enter_setquantity("10");
	bsp.clickCalculate();
	bsp.clickPlaceOrder();
	TakeScreenShot.screenShot("stockpage");

	}
}
