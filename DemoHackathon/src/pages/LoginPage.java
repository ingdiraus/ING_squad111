package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import BasePage.BasePage;

public class LoginPage extends BasePage{
	
	public LoginPage(WebDriver driver) {
		super(driver);
	}
	@FindBy(id="login")
	private WebElement username;
	
	@FindBy(id="login")
	private WebElement password;
	
	@FindBy(id="login")
	private WebElement login_button;
	
	public void enter_username(String user){
		username.sendKeys(user);
	}
	public void enter_password(String pass){
		password.sendKeys(pass);
	}
	public void clickLogin(){
		login_button.click();
	}
	

}
