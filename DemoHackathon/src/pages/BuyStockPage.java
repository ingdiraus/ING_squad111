package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import BasePage.BasePage;

public class BuyStockPage extends BasePage{
	
	@FindBy(id="value")
	private WebElement selectstock;
	
	@FindBy(id="login")
	private WebElement setquantity;
	
	@FindBy(id="login")
	private WebElement calculate;
	@FindBy(id="login")
	private WebElement placeorder;
	
	public void selectstock(String name){
		Select select = new Select(selectstock);
		select.selectByVisibleText(name);
	}
	public void enter_setquantity(String quantity){
		setquantity.sendKeys(quantity);
	}
	public void clickCalculate(){
		calculate.click();
	}
	
	public void clickPlaceOrder(){
		placeorder.click();
	}
	


	public BuyStockPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

}
