package BaseTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest {
	
	public WebDriver driver ;
	
	@BeforeMethod
	public void login(){
		
		System.setProperty("driver.chrome.driver", "D:\\Users\\Hackathon\\heckathonworkspace1\\DemoHackathon\\src\\hackathon\\softwares\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.get("http://10.117.189.57:2015/views/#!/stock");
	}
	
	@AfterMethod
	public void closepage(){
		driver.close();
	}

}
