package Utilities;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import BaseTest.BaseTest;

public class TakeScreenShot {
static WebDriver driver;
	public   static void screenShot(String screenshotname) throws IOException{
		TakesScreenshot ts=(TakesScreenshot)driver;
	File srcfile =ts.getScreenshotAs(OutputType.FILE);
	FileUtils.copyFile(srcfile, new File("D://selenium//"+screenshotname+".png"));
		
	}

}
